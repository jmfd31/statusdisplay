﻿//#define ENTW 

using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Display
{
    struct Color
    {
        public ushort color;

        public Color(ushort color)
        {
            this.color = color;
        }

        public System.Drawing.Color DColor
        {
            get
            {
                return System.Drawing.Color.FromArgb(red, green, blue);
            }
        }

        public byte imgEncoding1
        {
            get
            {
                return (byte)color;
            }
        }

        public byte imgEncoding2
        {
            get
            {
                return (byte)(color >> 8);
            }
        }

        public byte red
        {
            get
            {
                return (byte)((color & 0xF800) >> 8);
            }
            set
            {
                color = (ushort)(color & 0x07FF | (0xF800 & (value << 8)));
            }
        }

        public byte green
        {
            get
            {//0000 0111 1110 0000
                return (byte)((color & 0x07E0) >> 3);
            }
            set
            {
                color = (ushort)(color & 0xF81F | (0x07E0 & (value << 3)));
            }
        }

        public byte blue
        {
            get
            {
                return (byte)((color & 0x001F) << 3);
            }
            set
            {
                color = (ushort)(color & 0xFFE0 | (0x001F & (value >> 3)));
            }
        }

    }

    class Dispaly
    {
        private SerialPort _serialPort;

        public Dispaly() : this("COM5") { }
        public Dispaly(string portName)
        {
            // Create a new SerialPort object with default settings.
            _serialPort = new SerialPort();

            // Allow the user to set the appropriate properties.
            _serialPort.PortName = portName;
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.None;
            _serialPort.DataBits = 8;
            _serialPort.StopBits = StopBits.Two;
            _serialPort.Handshake = Handshake.None;

            // Set the read/write timeouts
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            _serialPort.Open();

            //Welcome-Msg des Displays lesen
            try
            {
                while (true)
                {
                    Console.Write("Clear Serial Port...\n");
                    Console.Write(_serialPort.ReadChar());
                }
            }
            catch (Exception)
            {
                Console.WriteLine("\n\nStart\n");
            }
        }

        public Dispaly(SerialPort serialPort)
        {
            this._serialPort = serialPort;
        }
        
        public void closeSerialPort()
        {
            _serialPort.Close();
        }

        public virtual void drawPix(int x, int y, Color c)
        {
            string cmd = string.Format("P{0,3},{1,3},0x{2:X4}", x, y, c.color);
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
        }

        public virtual void fillRect(int x, int y, int w, int h, Color c)
        {
            string cmd = string.Format("R{0,3},{1,3},{2,3},{3,3},0x{4:X4}", Math.Min(x, 239), Math.Min(y, 319), Math.Min(x + w, 239), Math.Min(y + h, 319), c.color);
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
        }

        public virtual void drawRect(int x, int y, int w, int h, int border, Color c)
        {
            string cmd = string.Format("C{0,3},{1,3},{2,3},{3,3},{4,3},0x{5:X4}", Math.Min(x, 239), Math.Min(y, 319), Math.Min(x + w, 239), Math.Min(y + h, 319), border, c.color);
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
        }

        public virtual void drawText(int x, int y, Color fgColor, Color bgColor, string text, bool bold)
        {
            string cmd = string.Format("T{0,3},{1,3},{2,3},{3,3},0x{4:X4},0x{5:X4},{6}\0", Math.Min(x, 239), Math.Min(y, 319), 1, bold ? 1 : 0, fgColor.color, bgColor.color, text);
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
        }
        public void testLcd()
        {
            string cmd = string.Format("S");
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
        }

        public virtual void drawImage(int x, int y, Color[,] image)
        {
            int w = image.GetLength(0);
            int h = image.GetLength(1);

            byte[] data = new byte[image.Length * 2];

            int i = 0;
            for (int y_ = 0; y_ < image.GetLength(1); y_++)
            {
                for (int x_ = 0; x_ < image.GetLength(0); x_++)
                {
                    data[i++] = image[x_, y_].imgEncoding1;
                    data[i++] = image[x_, y_].imgEncoding2;
                }
            }

            drawImage(x, y, w, h, data);
        }

        internal void drawImage(int x, int y, int w, int h, byte[] data)
        {
            string cmd = string.Format("I{0,3},{1,3},{2,3},{3,3}", Math.Min(x, 239), Math.Min(y, 319), w, h);
            if (sendCommand(cmd) != 'A') throw new Exception("Display hat Fehler gemeldet");
            sendData(data);
        }

        public virtual void drawLine(int x1, int y1, int x2, int y2, Color c)
        {
            int w = x2 - x1;
            int h = y2 - y1;
            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
            if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
            if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                drawPix(x1, y1, c);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x1 += dx1;
                    y1 += dy1;
                }
                else
                {
                    x1 += dx2;
                    y1 += dy2;
                }
            }
        }

        public virtual void clear()
        {
            fillRect(0, 0, 239, 319, new Color());
        }

        private static void Swap(ref int x, ref int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }
        public void syncLcd()
        {
            int i = 0;
            while (true)
            {
                try
                {
                    testLcd();
                    Console.WriteLine("Sync after " + i + " fails");
                    return;
                }
                catch (Exception)
                {
                    i++;
                }
            }
        }

        private void sendData(byte[] data)
        {
            Console.Write("Send: ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(data.Length + " Bytes raw data");
            Console.ResetColor();
            Console.Out.Flush();

#if ENTW
            for (int i = 0; i < data.Length; i++)
            {
                _serialPort.Write("" + data[i]);
                string res;
                if (i < data.Length - 1 && !string.IsNullOrEmpty(res = _serialPort.ReadExisting()))
                {
                    Console.WriteLine("Got data Response...? " + i + ", " + res);
                }
                if ((i + 1) % 500 == 0)
                {
                    Console.Out.Write(".");
                    Console.Out.Flush();
                }
            }
            Console.WriteLine();
            _serialPort.Write("\n");
#else
            _serialPort.Write(data, 0, data.Length);
            _serialPort.Write("\n");
#endif
        
            
        }

        private char sendCommand(string cmd)
        {
            Console.Write("Send: ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            if (ToLiteral(cmd).Length > 40)
            {
                Console.Write(ToLiteral(cmd).Substring(0, 37) + "... (" + cmd.Length + " Zeichen)");
            }
            else
            {
                Console.Write(ToLiteral(cmd));
            }
            Console.ResetColor();
            Console.Out.Flush();

#if ENTW
            for (int i = 0; i < cmd.Length; i++)
            {
                _serialPort.Write("" + cmd[i]);
                string res;
                if (i < cmd.Length - 1 && !string.IsNullOrEmpty(res = _serialPort.ReadExisting()))
                {
                    Console.WriteLine("Got early Response...? " + i + ", " + res);
                }
                if ((i+1) % 500 == 0)
                {
                    Console.Out.Write(".");
                    Console.Out.Flush();
                }
            }
            _serialPort.Write("\n");
#else
            _serialPort.WriteLine(cmd);
#endif
            char result = (char)_serialPort.ReadChar();
            Console.Write("\tRecive: ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(result);
            Console.ResetColor();
            Console.Out.Flush();
            return result;
        }

        private string ToLiteral(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }
    }
}
