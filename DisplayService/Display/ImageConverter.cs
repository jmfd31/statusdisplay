﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Display
{
    class ImageConverter
    {
        public static Color[,] Convert(Image i)
        {
            Color[,] img = new Color[i.Width, i.Height];

            using(Bitmap bmp = new Bitmap(i))
            {
                for(int x = 0; x < i.Width; x++)
                {
                    for(int y = 0; y < i.Height; y++)
                    {
                        System.Drawing.Color c = bmp.GetPixel(x, y);
                        img[x, y] = convertColor(c);
                    }
                }
            }
            return img;
        } 

        private static Color convertColor(System.Drawing.Color cIn)
        {
            Color cOut = new Color();
            cOut.red = cIn.R;
            cOut.green = cIn.G;
            cOut.blue = cIn.B;

            //Console.WriteLine(String.Format("In: R={0:X}, G={1:X}, B={2:X}, Out: R={3:X}, G={4:X}, B={5:X}, v={6:X}", cIn.R, cIn.G, cIn.B, cOut.red, cOut.green, cOut.blue, cOut.color));
            return cOut;
        }
    }


}
