﻿//#define ENTW 

using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Display
{

    class OffscreenDisplay:Dispaly
    {
        private readonly Pen P = new Pen(System.Drawing.Color.White);
        private readonly SolidBrush B = new SolidBrush(System.Drawing.Color.Black);
        private readonly Font F = new Font("Consolas", 12);


        private int _w;
        private int _h;

        private Bitmap _screen;

        public Graphics PaintArea { get { return Graphics.FromImage(_screen); } }
        public Graphics PaintAreaAA
        {
            get
            {
                Graphics g = PaintArea;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                return g;
            }
        }


        public OffscreenDisplay(string portName) :this(portName, 240,320) { }

        public OffscreenDisplay(string portName, int w, int h) : base(portName)
        {
            this._w = w;
            this._h = h;

            this._screen = new Bitmap(_w, _h, PixelFormat.Format16bppRgb565);
        }

        public override void drawPix(int x, int y, Color c)
        {
            drawPix(x, y, c.DColor);
        }

        public void drawPix(int x, int y, System.Drawing.Color c)
        {
            _screen.SetPixel(x, y, c);
        }
        public override void fillRect(int x, int y, int w, int h, Color c)
        {
            fillRect(x, y, w, h, c.DColor);
        }

        public void fillRect(int x, int y, int w, int h, System.Drawing.Color c)
        {
            using (Graphics g = PaintArea)
            {
                B.Color = c;
                g.FillRectangle(B, x, y, w, h);
            }
        }

        public void drawOval(int x, int y, int w, int h, System.Drawing.Color c)
        {
            using (Graphics g = PaintAreaAA)
            {
                P.Color = c;
                g.DrawEllipse(P, x, y, w, h);
            }
        }

        public void drawArc(int x, int y, int w, int h, float startAngle, float sweepAngle, System.Drawing.Color c)
        {
            using (Graphics g = PaintAreaAA)
            {
                P.Color = c;
                g.DrawArc(P, x, y, w, h, startAngle, sweepAngle);
            }
        }

        public override void drawRect(int x, int y, int w, int h, int border, Color c)
        {
            drawRect(x, y, w, h, border, c.DColor);
        }

        public void drawRect(int x, int y, int w, int h, int border, System.Drawing.Color c)
        {
            using (Graphics g = PaintArea)
            {
                P.Color = c;
                g.DrawRectangle(P, x, y, w, h);
            }
        }

        public override void drawText(int x, int y, Color fgColor, Color bgColor, string text, bool bold)
        {
            drawText(x, y, fgColor.DColor, bgColor.DColor, text, bold);
        }

        public void drawText(int x, int y, System.Drawing.Color fg, System.Drawing.Color bg, string text, bool bold)
        {
            using (Graphics g = PaintAreaAA)
            {
                B.Color = fg;
                g.DrawString(text, F, B, x, y);
            }
        }

        public override void drawImage(int x, int y, Color[,] image)
        {
            for(int x_ = 0; x_ < image.GetLength(0); x++)
            {
                for(int y_ = 0; y_ < image.GetLength(1); y_++)
                {
                    drawPix(x_, y_, image[x_, y_]);
                }
            }
        }

        public void drawImage(int x, int y, Image image)
        {
            using (Graphics g = Graphics.FromImage(_screen))
            {
                g.DrawImage(image, x, y);
            }
            int w = image.Width;
            int h = image.Height;
        }

        public override void drawLine(int x1, int y1, int x2, int y2, Color c)
        {
            drawLine(x1, y1, x2, y2, c.DColor);
        }

        public void drawLine(int x1, int y1, int x2, int y2, System.Drawing.Color c)
        {
            using (Graphics g = PaintAreaAA)
            {
                P.Color = c;
                g.DrawLine(P, x1, y1, x2, y2);
            }
        }

        public override void clear()
        {
            fillRect(0, 0, _w, _h, new Color());
        }

        public void repaint()
        {

            var data = _screen.LockBits(new Rectangle(0, 0, _w, _h), ImageLockMode.ReadOnly, PixelFormat.Format16bppRgb565);
            var len = data.Stride * data.Height;

            byte[] bytes = new byte[len];

            Marshal.Copy(data.Scan0, bytes, 0, len);
            _screen.UnlockBits(data);

            base.drawImage(0, 0, _w, _h, bytes);
        }
    } 
}
