﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace App.TimeMonitor
{
    class Uhr : App
    {
        private const int STEPS = 12;

        private static readonly Color GRID_COLOR = Color.White;
        private static readonly Color SECONDS_COLOR = Color.YellowGreen;
        private static readonly Color MINUTE_COLOR = Color.LightGreen;
        private static readonly Color HOUR_COLOR = Color.Red;

        private static readonly Pen P_GRID = new Pen(GRID_COLOR, 1);
        private static readonly Pen P_SECOND = new Pen(SECONDS_COLOR, 3);
        private static readonly Pen P_MINUTE = new Pen(MINUTE_COLOR, 3);
        private static readonly Pen P_HOUR = new Pen(HOUR_COLOR, 3);

        private static readonly Brush B_MINUTE = new SolidBrush(MINUTE_COLOR);
        private static readonly Brush B_HOUR = new SolidBrush(HOUR_COLOR);

        public void Draw(Graphics g, int x, int y, int w, int h)
        {
            int rx = w / 2;
            int ry = h / 2;
            int cx = x + rx;
            int cy = y + ry;
            drawTime(g, x, y, w, h, cx, cy, rx, ry);
        }

        private static void drawTime(Graphics g, int x, int y, int w, int h, int cx, int cy, int rx, int ry)
        {
            long now = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            drawGrid(now, g, x, y, w, h, cx, cy, rx, ry);
            drawSecond(now, g, x, y, w, h, cx, cy, rx, ry);
            drawMinute(now, g, x, y, w, h, cx, cy, rx, ry);
            drawHour(now, g, x, y, w, h, cx, cy, rx, ry);
        }

        private static void drawGrid(long now, Graphics g, int x, int y, int w, int h, int cx, int cy, int rx, int ry)
        {
            for(int i = 0; i < STEPS; i++)
            {
                double angle = (2 * Math.PI / STEPS * i);

                int x1 = 0, y1 = 0;
                int x2 = 0, y2 = 0;

                calcCord(angle, ref x1, ref y1, 1.0d, cx, cy, rx, ry);
                calcCord(angle, ref x2, ref y2, 0.9d, cx, cy, rx, ry);

                g.DrawLine(P_GRID, x1, y1, x2, y2);
            }
            g.DrawEllipse(P_GRID, x, y, w, h);
        }
        private static void drawSecond(long now, Graphics g, int x, int y, int w, int h, int cx, int cy, int rx, int ry)
        {
            double s = now / 1000d;
            float angle = (float)((s % 60) * (360d / 60d));

            g.DrawArc(P_SECOND, x, y, w, h, angle - 10f, 20f);
        }

        private static void drawHour(long now, Graphics g, int x, int y, int w, int h, int cx, int cy, int rx, int ry)
        {
            double h_ = now / 1000d / 60d / 60d;
            double angle = (h_ % 12)*(2*Math.PI/12d);

            int x2=0, y2=0;

            calcCord(angle, ref x2, ref y2, 0.6d, cx, cy, rx, ry);

            g.DrawLine(P_HOUR, cx, cy, x2, y2);
            g.FillEllipse(B_HOUR, x2 - 4, y2 - 4, 8, 8);
        }

        private static void drawMinute(long now, Graphics g, int x, int y, int w, int h, int cx, int cy, int rx, int ry)
        {
            double min = now / 1000d / 60d;
            double oneMin = (2 * Math.PI / 60d);
            double angle = (min % 60) * oneMin;

            int x2 = 0, y2 = 0, x1 = 0, y1 = 0;

            calcCord(angle, ref x2, ref y2, 0.8d, cx, cy, rx, ry);

            g.DrawLine(P_MINUTE, cx, cy, x2, y2);
            g.FillEllipse(B_MINUTE, x2 - 4, y2 - 4, 8, 8);

            for(int i = (int)min - 3; i<= min + 3; i++)
            {
                calcCord(oneMin*i, ref x1, ref y1, i%5==0?0.8d:0.9d, cx, cy, rx, ry);
                calcCord(oneMin*i, ref x2, ref y2, 1.0d, cx, cy, rx, ry);

                g.DrawLine(P_GRID, x1, y1, x2, y2);
            }
        }

        private static void calcCord(double angle, ref int x2, ref int y2, double lengMult, int cx, int cy, int rx, int ry)
        {
            x2 = cx + (int)(Math.Sin(angle) * rx * lengMult);
            y2 = cy - (int)(Math.Cos(angle) * ry * lengMult);
        }
    }
}
