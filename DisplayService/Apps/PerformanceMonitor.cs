﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App.PerformanceMonitor
{
    class PerformanceMonitor
    {
        private const int ELEMENTS = 240;

        private PerformanceCounter _cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        private PerformanceCounter _ramCounter = new PerformanceCounter("Memory", "% Committed Bytes In Use");

        private float[] _cpuHistory = new float[ELEMENTS];
        private float[] _ramHistory = new float[ELEMENTS];

        public float[] CpuHistory { get { return _cpuHistory; } }
        public float[] RamHistory { get { return _ramHistory; } }

        public float Cpu { get { return _cpuHistory[ELEMENTS - 1]; } }
        public float Ram { get { return _ramHistory[ELEMENTS - 1]; } }

        private bool _stop = false;

        public PerformanceMonitor()
        {
            new Thread(read).Start();
        }

        public void Stop()
        {
            this._stop = true;
            _cpuCounter.Dispose();
            _ramCounter.Dispose();
        }

        private void read()
        {
            while (!_stop)
            {
                for(int i = 0; i < _cpuHistory.Length-1; i++)
                {
                    _cpuHistory[i] = _cpuHistory[i + 1];
                }
                for (int i = 0; i < _ramHistory.Length - 1; i++)
                {
                    _ramHistory[i] = _ramHistory[i + 1];
                }
                _cpuHistory[_cpuHistory.Length - 1] = getCurrentCpuUsage();
                _ramHistory[_ramHistory.Length - 1] = getAvailableRAM();
                Thread.Sleep(1000);
            }
        }

        private float getCurrentCpuUsage()
        {
            return _cpuCounter.NextValue();
        }

        private float getAvailableRAM()
        {
            return _ramCounter.NextValue();
        }
    }
}
