﻿using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.AudioMonitor
{
    class AudioDeviceDisplay : App
    {
        private const string HEADSET_RAW = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABbpJREFUeNrEl19oFFkWh7/qqv5TXb3RYHda3c4m4kASk2CmRwaEbYQssyQLIotIYAb3RVhGmAdhn0R2HuZpRFlcFgYfZtGnwRD0YaM4IAw7ow0LaSWBgKYfOqKEGDWY7qTr1p+uqn2p7q3UxN6EVbbgUE3V/d3z9Tn3nHtL8jyP/+clnTp1alsDs9msDBwFDvmP+oBUYMgS8AKoAg9WVlaWtjOv0u6lqqq/950erdVqR9PptNzR0QFAKpVCUf4jNwwDwzCwbZvXr1+jquoi8AC4B0wKIawdARiG8U1XV9fZdDpNJpMhk8lQq9Wo1+sA6LqOEKI1Ph6Ps3v3bhRFob+/H9u2D7x8+fLA8vLy6UqlMpZIJD7bNkAkErlYrVbPZrNZhBDMzc2xsrKCaZpYloXnedi2HRyPoigoioIsyy2YdDpNV1cXpVLp0/37978A/rQdgIFYLPb52tqad/fuXUkIgWmar4Dv/ZA+BJyhoaHZpmB+fn4X8IFve4FR4Jiqqrs0TUPXdYA/At8Cj9sCRKPR7zRNw3XdmcXFxXvA94VC4UG7tTI0NFT1wR76j/4KcP/+/THgU0mSJgYHBz3Xdb+zbfvDdlWQlWV5EdAdx0m/y3KTZXkF+IXjOAeAlVb6QuNGNU1rAD++h5L/pz/36Kb1tikckjQiSVLScZyH79q74zgzQEKSpJG3AgDdpmnawMZ7iEDVNE0L6H4rgOd5Gdu2JUB+DwAx27Yjnudl2gFgmmYc2PUeAHZZlhUL7z0/AxBC4Hner961d8/zeoUQclsAoCaEwHXdwe7u7m+6u7sbvv1tpw7Detd1D/utu9YuAvVcLkehUBixLOvs8PCwrGmabFnWFzsFCOsLhcJILpfD87z6lp1wYmIitbq6mu/v72fv3r2xy5cvY5omtm1jmuaOQ26a5ib92NhYLJ/P8+TJk/yePXtSk5OTG5sALMu6lc/nBw8ePIjruq2N538BCOqTySTDw8NomjZYLpdvAb9tpWB8fHwik8l80tPTg23byLLc2t8ty8IwjB0DhPWO42DbNgMDA2Sz2U/Gx8cnWhEwDOP0vn370HUdRVFa1WAYBqZpIoRgeno6Fmya/23RHz9+fJO+CaDrOrlcjoWFhdPApAIghPgokUig6zrRaBRZlhFC4G/FzYNHh+/YC923vML6ZhobjQaapiGE+KgVASGE3Gg0EEJgWRaRSGQrAK3573z72fkyACSF9bqu02g0UBSFZDKJEEIOpuDhmzdvxpqEbwFwfMduMNRBp4HfSlhfr9cxTRNVVdF1HcMwHgFEpqampHq9PlUul4lGowghWF9f3wpA+Cfepq2F7k2rARth/cbGBkIIFEXh8ePHVKvVm1NTU3IEkK9du/aPYrE4W6lUSKVSOI6zFUDDhzC2MBEwHXDCes/zSKVSlMtlisXi3PXr16cBWQFia2trHXfu3PmyXq//5dixYx/09va2VnCgDOP+v2vl/9y5c4TSwZUrVyQgHtYnEgnm5+cplUpMT09/tb6+3gHUFD9vsaWlJXHjxo0/P3v2bCyTyXxsmuZAs4H4K1gOrP62HzuA3OyETf3Vq1efrK6uzh0+fPjE8vKyAcSafSgB/BIYAX4N/AYYU1X1p2Qy6SWTSU9V1XvAHh9CBqL+BHHfYv6z5vuMqqo/BPQ/ASdOnjz59zNnzvwIfAz0AqrkU8SBpH+P+tURDWxWOvAKsPzn0UuXLt2KRqOjjx49Ip/PU6/XL164cOFrv1riQNovXenQoUPpI0eO/K6zs/MPpVLpRLFYXADWARFsJM0QRwLWfO8Atj8mDsTPnz//anZ2lr6+PhYWFujr62t3FnjhOM6/nj59ev727dvP/bkcwAl+FzjbyG2kCVOpVH7o7OwcnZmZoaenh+fPn1+8efPm1361uIG10uwdbqCJtZqZtN3Pc0mSCEUo2Hi8gKNwp/TCp67g9e8BALZhZrYxMwYTAAAAAElFTkSuQmCC";
        private const string SPEAKER_RAW = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAeLSURBVHjaYvz//z/DQAKAAGJiGGAAEEAs2AQZGRlJM4VNmIFBIZ2BgZmTgYEFiJnYxBmZmRYwMLKY///xaSPDzYZ0hj/ffjEImTH8f3sSRStAAFEeAizcDAwyYQj+3/8CfOy/NmQ4/fOoDeEUlJYSSWAQcm4Ey707haEdIIAocwAjULuUP9DnXFDL/zGyszMsDDL9Z6Es8odBS5aVIcdPkIFJ3K6IgVVAFZsRAAFEmQPE3BgYWAUZGP7/A+L/DEzM/zuzwtT9xLm+Mnz9+Z/h8/d/DEYq3AyG2hJsDByaYdiMAAgg8h0gYMTAwCkDtPgvhP/je3JZtlupoRyQ+fMHg6AgLwMoh7GwMDE4GwIdyaXkBgozdGMAAog8B3DJMzDwagAt/wMx8+d3O1EDi6kpEZYM169cYhCX4GeQlRUBO+LHz78MBqq8DCyCEpoMjGzi6EYBBBDpDmAD+kbAAGo5EPz+ocgkqr6isSqEnenrG4Y3b54Dfc3M8O/ffwZhYQFQzDDIiHIySIgLizAw8mKkA4AAIs0BzBwMDPyGkDgHJ7rf3Axc4qv8spIknXSZGe7ff8Dw48dnYDZmAlr8j4GDg5WBnYONgYeThUFeip+RgYlHBd1IgABiIZjKQSmclR/icxBmYIbIgRzBxDnXKj7LRFeLj4ERmBSePH7A8PfPb4j0f0h5wsnJAU4HsuI8QAEuOXQrAAIIuwO4gImLhQ9oATvEAczsEMdAUzuQAAb9/0a1yMxwUytZhisPGBj+qTEwPHv6GCWZgZSys7OBHSIpAjKHQxLdKoAAwh4FIHWsvBAHgCwDxTc4tUPrjZ+/oiRcEupsvIwYbjxiYDh9HZghgAHz8tULBlZg/LOwIPwFSg8gVwnzA6OPiVUI3SqAAMLuAJBl4HhGqqhA/D+/GBi+f7XmMwmabR/uw/D2PQPDvjNAy1mBPgWa9P7DV4Y7d58yPHjwkIGVlRWcDZmZmcAhwMsNVMTEwotuFUAA4U4Df4Fx+RfkEEYZYBToMXAImDIIypuLqpnZOCSmcoHC5vANkDQDg5wIUOmfvwycHIIMVpauDOcvHGeQkpIEYilwggQBTjYgzcTIgW4NQABhd8D/f05MvBJhHOKqJrwy2moC8lq8QrLKDEJiEgx6CkwMoHR24BowTQI9JSXGwCANDNhfP38DExw3Q2JSBIP7M2uGvr4eBkdHBwYmJkZwQDKD3fEXoyACCCCsDhDScVmqH9cgISTEDcxKwOTACImVf8Ck8PYDA8O155DIkwRaDCxnGASBCfz33//gRHft6l0GZRVZcPC/f/+egYeHD5wlIQn4D4YDAAIIqwOEZRX5tJS5GV69BVZgwHj+B9QLLFcYQOmJC+hrNjYGBm5OUDaDOICDHeQARoZfv38zXL58i4GHlxeY+jkYvn79Bg6Bf8B4+guKzr8/f6PbBRBAWB3A9vfjD3UBBi5QIgMlaBZGSInLxATJZXxcEMvBBvyD5NBffyGKvnz5xvDhw2eG379/AUOPHRwSjED4/cdPoCu/f0G3CyCAsOaCn++fPLMCFgXAGGBgAzqAlRVCs4DTESTuQXwY/v0PVCywgF31DxhPt27dAfr4D7AuEIL4HAg+fwba/efbW3S7AAIIqwOe37t5lf3vLwZhTgZwmQ4K//9A/PfXH4bf336CQ+E/VAwUP19//Gf4+Q+Y/1nZwD7ft28Hg76+DjCq2MD6QZn53Xtg4vn/9QW6XQABhNUBXx9ePXz91kMGSaADvn78zvDz03eGHx++MXwH5vOvLz8CHfGL4RfQIb++/GD4DcQf3n9n+Ab06OevXxhOn9oLrIRYGczNLRh+/vwJdMA/MH7x8jXQ5O+P0O0CCCDs2fDPiz07tu35Fp2myvX5ySeG3yxAX/wFGvTnH9ghzMygQuYPMHiBYkD8DejL95pcDBw8XMBqmJ/B2sYKbCnYKGD58P8fI8OTp0+AAfHrHrpVAAGEqyC6uXbxjL1Kyia+H6+9Znj5/QfQor/AwuYPw68f34G5Q55BQEyJgf3vd7DYjx9/GB7riDKIyqgxfHt5EWjpH0jiA8bVn99/GL4BC47Hjx68A5p7H90igADCWRJ+fH6poCbd7h0DMysz0AufgfgbKBKBefkDsKJSZ1aLTxOW9mLk/AGMpk+fGS4Bfa6vrQLMESwolRHIMS9ff2B49vghyPIn6PYABBC+6hgYXD8SGP7+wBJF7xj+Xutf++r5wQnMQkFa/14IMxzfcIlB19iBgY1LEFxqgcr/30Df/wNmzxu37jL8/PjsDKjhhm4UQABR0ijdzfD+nPXfR21T/3NsYrj/8DLD4ycsDBxCKuDaE1QJffz4BRgC/xiOnwQ1x3/sxWYIQABR2i/4AMwSOQxvt/v8+z751vHDKxiYJByBFeZvhk+fvgKL4i8Mr968ZTh29NBDoNqD2AwACCBqdc22Mvx5Y3VhY/GsE8f2Mbz5Kcjw4tk7YEnIwbBt+w6GD68fzgOqeY1NI0AAgVMrOqYQBKprm72sqWn7n5Vd/J+dg3s/qHrBZR9AANHCASCgzcHBNQVIdwCxPD4PAwQQ40B3zwECDAAXueRHsP4MbAAAAABJRU5ErkJggg==";

        private static readonly Image HEADSET = Base64ToImage(HEADSET_RAW);
        private static readonly Image SPEAKER = Base64ToImage(SPEAKER_RAW);

        private static readonly Color BOX_COLOR = Color.White;
        private static readonly Color BAR_COLOR = Color.LightGreen;

        private static readonly Pen P = new Pen(BOX_COLOR, 1);
        private static readonly Brush B = new SolidBrush(BAR_COLOR);


        public AudioDeviceDisplay() { }

        void App.Draw(Graphics g, int x, int y, int w, int h)
        {
            var enumerator = new MMDeviceEnumerator();
            var ep = enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);
            var name = ep.FriendlyName;
            if (name.Contains("Kopfh"))
            {
                g.DrawImage(HEADSET, x, y);
            } else
            {
                g.DrawImage(SPEAKER, x, y);
            }
            var volume = ep.AudioEndpointVolume.MasterVolumeLevelScalar;
            var volPix = (int)(volume * (h - 2));

            g.DrawRectangle(P, x + w - 6, y, 6, h);
            g.FillRectangle(B, x + w - 5, y + h - volPix - 1, 4, volPix);
        }
        
        private static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            Image image = Image.FromStream(ms, true);
            return image;
        }
    }
}
