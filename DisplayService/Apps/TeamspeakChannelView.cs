﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace App.TeamspeakMonitor
{

    class TeamspeakChannelView : ActiveApp
    {
        private const int NAMES_PER_ROW = 3;
        private const int ROW_COUNT = 2;
        private const int CUT_NAME_LEN = 7;

        private const string API_KEY = "U0MD-8YW4-77QZ-GFVV-NV8E-S3YX";

        private static readonly Color TEXT_COLOR = Color.White;
        private static readonly Brush B = new SolidBrush(TEXT_COLOR);
        private static readonly Font FONT = new Font("Consolas", 12);

        private Thread _thread;
        private bool _run = true;

        private List<string> _users = new List<string>();
        private string _channelName = "<ChannelName>";

        public TeamspeakChannelView()
        {
            _thread = new Thread(run);
            _thread.Name = "TeamSpeak-Telnet";
            _thread.Start();
        }

        private void run()
        {
            while (_run)
            {
                Socket s = new Socket(SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    string cid = "", clid = "";
                    string channel = "", channelData = "";
                    List<string> userList = new List<string>();

                    connect(s);

                    while (_run)
                    {
                        getClientInfo(s, ref cid, ref clid);
                        getChannelName(s, ref channel);
                        getChannelData(s, cid, ref channelData);
                        extractUsers(channelData, ref userList);

                        lock (this)
                        {
                            _users.Clear();
                            _users.AddRange(userList);
                            _channelName = channel;
                        }
                        Thread.Sleep(1000);
                    }
                }
                catch (Exception)
                {
                    lock (this)
                    {
                        _channelName = "";
                        _users.Clear();
                    }
                    Thread.Sleep(5000);
                }
                try
                {
                    s.Disconnect(false);
                    s.Close();
                }
                catch (Exception) { }
                lock (this)
                {
                    _channelName = "";
                    _users.Clear();
                }
                Thread.Sleep(1000);
            }
        }

        private void connect(Socket s)
        {
            s.Connect("127.0.0.1", 25639);
            s.ReceiveTimeout = 500;
            s.SendTimeout = 500;

            recive(s); //Wilkommensnachricht lesen und verwerfen

            send(s, "auth apikey="+API_KEY+"\n");
            recive(s);
        }

        private void getClientInfo(Socket s, ref string cid, ref string clid)
        {
            send(s, "whoami\n");
            string whoAmI = recive(s);
            clid = getParameterValue(whoAmI, "clid");
            cid = getParameterValue(whoAmI, "cid");
        }

        private void getChannelName(Socket s, ref string channel)
        {
            send(s, "channelconnectinfo\n");
            channel = recive(s);

            channel = getParameterValue(channel, "path").Replace("\\s", " ");
            int index = channel.LastIndexOf("\\/");
            if (index > 0) index += 2; else index = 0;
            channel = channel.Substring(index);
        }

        private void getChannelData(Socket s, string cid, ref string channelData)
        {
            send(s, "channelclientlist cid=" + cid + "\n");
            channelData = recive(s);
        }

        private void extractUsers(string channelData, ref List<string> userList)
        {
            var regex = new Regex("client_nickname=(.*?) client_type=");
            MatchCollection matches = regex.Matches(channelData);

            userList.Clear();

            foreach (Match match in matches)
            {
                string name = match.Groups[1].Value;
                int index = name.LastIndexOf("\\s");
                if (index > 0) index += 2; else index = 0;
                userList.Add(name.Substring(index));
            }
        }

        private string getParameterValue(string msg, string parameter)
        {
            int start = msg.IndexOf(parameter);
            int end = Math.Min(msg.IndexOf(" ", start), msg.IndexOf("\n", start));
            return msg.Substring(start + parameter.Length + 1, end - start - parameter.Length - 1);
        }

        private void send(Socket s, string msg)
        {
            byte[] data = Encoding.ASCII.GetBytes(msg);
            s.Send(data);
        }

        private string recive(Socket s)
        {
            byte[] buffer = new byte[1024];
            string result = "";
            try
            {
                while (true)
                {
                    int iRx = s.Receive(buffer);
                    if(iRx == 0)
                    {
                        return result;
                    }
                    result += Encoding.ASCII.GetString(buffer, 0, iRx);
                }
            }
            catch (Exception)
            {
                return result;
            }
        }

        private string buildNameString()
        {
            string result = "";
            int i = 0;

            if (_users.Count == 0)
            {
                return "---";
            }
            if(_users.Count <= ROW_COUNT)
            {
                foreach(string u in _users)
                {
                    result += u + "\n";
                }
                return result;
            }
            if(_users.Count <= NAMES_PER_ROW * ROW_COUNT)
            {
                foreach (string u in _users)
                {
                    i++;
                    result += u.Substring(0, Math.Min(CUT_NAME_LEN, u.Length)) + ", ";
                    if (i % NAMES_PER_ROW == 0) result += "\n";
                }
                return result.Substring(0, result.LastIndexOf(","));
            }
            foreach (string u in _users)
            {
                i++;
                result += u.Substring(0, Math.Min(CUT_NAME_LEN, u.Length)) + ", ";
                if (i % NAMES_PER_ROW == 0) result += "\n";
                if (i == 5) break;
            }
            result += "+" + (_users.Count - 5);
            return result;
        }

        public void Stop()
        {
            _run = false;
        }

        public void Draw(Graphics g, int x, int y, int w, int h)
        {
            lock (this)
            {
                g.DrawString(_channelName, FONT, B, x, y);
                g.DrawString(buildNameString(), FONT, B, x + 3, y + 20);
            }
        }
    }
}
