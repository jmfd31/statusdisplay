﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace App.PerformanceMonitor
{
    class PerformanceDisplay : ActiveApp
    {
        private const int GRID_SUB_LINES = 4;

        private static readonly Color GRID_COLOR = Color.White;
        private static readonly Color GRID_HELP_COLOR = Color.FromArgb(10,10,10);
        private static readonly Color CPU_COLOR = Color.Red;
        private static readonly Color MEM_COLOR = Color.Lime;

        private static readonly Pen P_GRID = new Pen(GRID_COLOR, 3);
        private static readonly Pen P_HELP_GRID = new Pen(GRID_HELP_COLOR, 1);
        private static readonly Pen P_MEM = new Pen(MEM_COLOR, 3);
        private static readonly Brush B_MEM = new SolidBrush(MEM_COLOR);
        private static readonly Brush B_CPU = new SolidBrush(CPU_COLOR);

        private static readonly Font FONT = new Font("Consolas", 12);

        private PerformanceMonitor _monitor;

        public PerformanceDisplay()
        {
            _monitor = new PerformanceMonitor();
        }

        void App.Draw(Graphics g, int x, int y, int w, int h)
        {
            drawGrid(g, x, y, w, h);
            drawCpu(g, x, y, w, h);
            drawMem(g, x, y, w, h);
        }

        private void drawGrid(Graphics g, int x, int y, int w, int h)
        {
            g.DrawLine(P_GRID, x, y, x + w, y);

            for(int i = 1; i < GRID_SUB_LINES; i++)
            {
                int y_ = y + h / GRID_SUB_LINES * i;
                g.DrawLine(P_HELP_GRID, x, y_, x + w, y_);
            }

            string cpuString = _monitor.Cpu.ToString("00") + "%";
            string memString = _monitor.Ram.ToString("00") + "%";

            g.DrawString(cpuString, FONT, B_CPU, x, y+2);
            g.DrawString(memString, FONT, B_MEM, x, y + 22);
        }

        private void drawMem(Graphics g, int x, int y, int w, int h)
        {
            float[] data = _monitor.RamHistory;
            for(int i = 1; i < data.Length; i++)
            {
                int a = (int)data[i - 1];
                int b = (int)data[i];

                g.DrawLine(P_MEM, x + i - 1, y + h - a, x + i, y + h - b);
            }
        }

        private void drawCpu(Graphics g, int x, int y, int w, int h)
        {
            float[] data = _monitor.CpuHistory;
            for(int i = 0; i < data.Length; i++)
            {
                g.FillRectangle(B_CPU, x + i, y+h-(int)data[i],1,(int)data[i]);
            }
        }

        void ActiveApp.Stop()
        {
            _monitor.Stop();
        }
    }
}
