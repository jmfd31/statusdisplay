﻿using App;
using App.AudioMonitor;
using App.PerformanceMonitor;
using App.TeamspeakMonitor;
using App.TimeMonitor;
using Display;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DisplayService
{
    public partial class Service1 : ServiceBase
    {
        private OffscreenDisplay _display;
        private bool active = false;
        private string _portName;
        private Dictionary<App.App, Rectangle> _apps = new Dictionary<App.App, Rectangle>();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            active = true;

            if (args.Length > 1)
            {
                _portName = args[0];
            }
            else
            {
                _portName = "COM5";
            }

            _apps.Add(new AudioDeviceDisplay(), new Rectangle(0,0,46,40));
            _apps.Add(new Uhr(), new Rectangle(240-140-5, 5, 140, 140));
            _apps.Add(new PerformanceDisplay(), new Rectangle(0,320-100,240,100));
            _apps.Add(new TeamspeakChannelView(), new Rectangle(0, 140, 240, 320 - 140 - 100));

            new Thread(mainLoop).Start();
        }

        private void mainLoop()
        {
            bool initDone = false;

            while (active)
            {
                try
                {
                    if (!initDone)
                    {
                        initDone = initDisplay(_portName);
                    }
                    _display.clear();
                    using (Graphics g = _display.PaintAreaAA)
                    {
                        foreach (var app in _apps)
                        {
                            app.Key.Draw(g, app.Value.X, app.Value.Y, app.Value.Width, app.Value.Height);
                        }
                    }
                    _display.repaint();
                    Thread.Sleep(1000);
                }
                catch (Exception)
                {
                    initDone = false;
                    Thread.Sleep(5000);
                }
            }
            try
            {
                _display.closeSerialPort();
            }
            catch (Exception) { }
        }

        private bool initDisplay(string portName)
        {
            if(_display != null)
            {
                try
                {
                    _display.testLcd();
                    //Keine Exception => LCD ist OK
                    return true;
                }
                catch (Exception)
                {
                    try { _display.closeSerialPort(); }
                    catch (Exception) { }
                }
            }

            try
            {
                _display = new OffscreenDisplay(portName, 240, 320);
                _display.testLcd();
                return true;
            } catch (Exception)
            {
                return false;
            }
        }

        protected override void OnStop()
        {
            active = false;
            foreach (var app in _apps)
            {
                ActiveApp aApp = app.Key as ActiveApp;
                if (aApp != null) aApp.Stop();
            }
        }
    }
}
